const Type_AddHabitat: String = "AddHabitat"


func validateAddHabitat(data) -> String:
	var err: String

	return ""


func createAddHabitat(HabitatName: String):
	return {"HabitatName": HabitatName}


const Type_AddMove: String = "AddMove"


func validateAddMove(data) -> String:
	var err: String

	return ""


func createAddMove(MoveName: String):
	return {"MoveName": MoveName}


const Type_AddSpecies: String = "AddSpecies"


func validateAddSpecies(data) -> String:
	var err: String

	return ""


func createAddSpecies(SpeciesName: String):
	return {"SpeciesName": SpeciesName}


const Type_AddStatus: String = "AddStatus"


func validateAddStatus(data) -> String:
	var err: String

	return ""


func createAddStatus(StatusName: String):
	return {"StatusName": StatusName}


const Type_AddType: String = "AddType"


func validateAddType(data) -> String:
	var err: String

	return ""


func createAddType(TypeName: String):
	return {"TypeName": TypeName}


const Type_AddTypeMatchup: String = "AddTypeMatchup"


func validateAddTypeMatchup(data) -> String:
	var err: String

	return ""


func createAddTypeMatchup(AtkType: String, DefType: String, Modifier: float):
	return {"AtkType": AtkType, "DefType": DefType, "Modifier": Modifier}


const Type_Error: String = "Error"


func validateError(data) -> String:
	var err: String

	return ""


func createError():
	return {}


const Type_GetHabitat: String = "GetHabitat"


func validateGetHabitat(data) -> String:
	var err: String

	return ""


func createGetHabitat():
	return {}


const Type_GetHabitatResponse: String = "GetHabitatResponse"


func validateGetHabitatResponse(data) -> String:
	var err: String

	return ""


func createGetHabitatResponse(Habitat):
	return {"Habitat": Habitat}


const Type_GetMoves: String = "GetMoves"


func validateGetMoves(data) -> String:
	var err: String

	return ""


func createGetMoves():
	return {}


const Type_GetMovesResponse: String = "GetMovesResponse"


func validateGetMovesResponse(data) -> String:
	var err: String

	return ""


func createGetMovesResponse(Moves):
	return {"Moves": Moves}


const Type_GetSpecies: String = "GetSpecies"


func validateGetSpecies(data) -> String:
	var err: String

	return ""


func createGetSpecies():
	return {}


const Type_GetSpeciesResponse: String = "GetSpeciesResponse"


func validateGetSpeciesResponse(data) -> String:
	var err: String

	return ""


func createGetSpeciesResponse(Species):
	return {"Species": Species}


const Type_GetStatus: String = "GetStatus"


func validateGetStatus(data) -> String:
	var err: String

	return ""


func createGetStatus():
	return {}


const Type_GetStatusResponse: String = "GetStatusResponse"


func validateGetStatusResponse(data) -> String:
	var err: String

	return ""


func createGetStatusResponse(Status):
	return {"Status": Status}


const Type_GetTraits: String = "GetTraits"


func validateGetTraits(data) -> String:
	var err: String

	return ""


func createGetTraits():
	return {}


const Type_GetTraitsResponse: String = "GetTraitsResponse"


func validateGetTraitsResponse(data) -> String:
	var err: String

	return ""


func createGetTraitsResponse(Traits):
	return {"Traits": Traits}


const Type_GetTypeMatchup: String = "GetTypeMatchup"


func validateGetTypeMatchup(data) -> String:
	var err: String

	return ""


func createGetTypeMatchup():
	return {}


const Type_GetTypeMatchupResponse: String = "GetTypeMatchupResponse"


func validateGetTypeMatchupResponse(data) -> String:
	var err: String

	return ""


func createGetTypeMatchupResponse(Matchups: Dictionary):
	return {"Matchups": Matchups}


const Type_HabitatEditEntry: String = "HabitatEditEntry"


func validateHabitatEditEntry(data) -> String:
	var err: String

	return ""


func createHabitatEditEntry(
	Chance: int, HabitatName: String, Index: int, MaxLevel: int, MinLevel: int, Species: String
):
	return {
		"Chance": Chance,
		"HabitatName": HabitatName,
		"Index": Index,
		"MaxLevel": MaxLevel,
		"MinLevel": MinLevel,
		"Species": Species
	}


const Type_HabitatRemoveEntry: String = "HabitatRemoveEntry"


func validateHabitatRemoveEntry(data) -> String:
	var err: String

	return ""


func createHabitatRemoveEntry(HabitatName: String, Index: int):
	return {"HabitatName": HabitatName, "Index": Index}


const Type_Load: String = "Load"


func validateLoad(data) -> String:
	var err: String

	return ""


func createLoad(Path: String):
	return {"Path": Path}


const Type_MoveEditAttribute: String = "MoveEditAttribute"


func validateMoveEditAttribute(data) -> String:
	var err: String

	return ""


func createMoveEditAttribute(AttributeName: String, MoveName: String, Value):
	return {"AttributeName": AttributeName, "MoveName": MoveName, "Value": Value}


const Type_MoveEditModifier: String = "MoveEditModifier"


func validateMoveEditModifier(data) -> String:
	var err: String

	return ""


func createMoveEditModifier(
	AtkAttribute: String,
	DefAttribute: String,
	Index: int,
	MoveName: String,
	Power: int,
	Status: String,
	Type: String
):
	return {
		"AtkAttribute": AtkAttribute,
		"DefAttribute": DefAttribute,
		"Index": Index,
		"MoveName": MoveName,
		"Power": Power,
		"Status": Status,
		"Type": Type
	}


const Type_MoveRemoveModifier: String = "MoveRemoveModifier"


func validateMoveRemoveModifier(data) -> String:
	var err: String

	return ""


func createMoveRemoveModifier(Index: int, MoveName: String):
	return {"Index": Index, "MoveName": MoveName}


const Type_RemoveHabitat: String = "RemoveHabitat"


func validateRemoveHabitat(data) -> String:
	var err: String

	return ""


func createRemoveHabitat(HabitatName: String):
	return {"HabitatName": HabitatName}


const Type_RemoveMove: String = "RemoveMove"


func validateRemoveMove(data) -> String:
	var err: String

	return ""


func createRemoveMove(MoveName: String):
	return {"MoveName": MoveName}


const Type_RemoveSpecies: String = "RemoveSpecies"


func validateRemoveSpecies(data) -> String:
	var err: String

	return ""


func createRemoveSpecies(SpeciesName: String):
	return {"SpeciesName": SpeciesName}


const Type_RemoveStatus: String = "RemoveStatus"


func validateRemoveStatus(data) -> String:
	var err: String

	return ""


func createRemoveStatus(StatusName: String):
	return {"StatusName": StatusName}


const Type_RemoveType: String = "RemoveType"


func validateRemoveType(data) -> String:
	var err: String

	return ""


func createRemoveType(TypeName: String):
	return {"TypeName": TypeName}


const Type_RemoveTypeMatchup: String = "RemoveTypeMatchup"


func validateRemoveTypeMatchup(data) -> String:
	var err: String

	return ""


func createRemoveTypeMatchup(AtkType: String, DefType: String):
	return {"AtkType": AtkType, "DefType": DefType}


const Type_RenameHabitat: String = "RenameHabitat"


func validateRenameHabitat(data) -> String:
	var err: String

	return ""


func createRenameHabitat(NewName: String, OldName: String):
	return {"NewName": NewName, "OldName": OldName}


const Type_RenameMove: String = "RenameMove"


func validateRenameMove(data) -> String:
	var err: String

	return ""


func createRenameMove(NewName: String, OldName: String):
	return {"NewName": NewName, "OldName": OldName}


const Type_RenameSpecies: String = "RenameSpecies"


func validateRenameSpecies(data) -> String:
	var err: String

	return ""


func createRenameSpecies(NewName: String, OldName: String):
	return {"NewName": NewName, "OldName": OldName}


const Type_RenameStatus: String = "RenameStatus"


func validateRenameStatus(data) -> String:
	var err: String

	return ""


func createRenameStatus(NewName: String, OldName: String):
	return {"NewName": NewName, "OldName": OldName}


const Type_Save: String = "Save"


func validateSave(data) -> String:
	var err: String

	return ""


func createSave(Path: String):
	return {"Path": Path}


const Type_SetTraitName: String = "SetTraitName"


func validateSetTraitName(data) -> String:
	var err: String

	return ""


func createSetTraitName(DecreasedAttribute: String, IncreasedAttribute: String, TraitName: String):
	return {
		"DecreasedAttribute": DecreasedAttribute,
		"IncreasedAttribute": IncreasedAttribute,
		"TraitName": TraitName
	}


const Type_SpeciesAddEvolution: String = "SpeciesAddEvolution"


func validateSpeciesAddEvolution(data) -> String:
	var err: String

	return ""


func createSpeciesAddEvolution(
	Condition: String, Id: int, Name: String, SpeciesName: String, Trigger: String, Value
):
	return {
		"Condition": Condition,
		"Id": Id,
		"Name": Name,
		"SpeciesName": SpeciesName,
		"Trigger": Trigger,
		"Value": Value
	}


const Type_SpeciesAddMove: String = "SpeciesAddMove"


func validateSpeciesAddMove(data) -> String:
	var err: String

	return ""


func createSpeciesAddMove(
	Condition: String, Id: int, Name: String, SpeciesName: String, Trigger: String, Value
):
	return {
		"Condition": Condition,
		"Id": Id,
		"Name": Name,
		"SpeciesName": SpeciesName,
		"Trigger": Trigger,
		"Value": Value
	}


const Type_SpeciesAddType: String = "SpeciesAddType"


func validateSpeciesAddType(data) -> String:
	var err: String

	return ""


func createSpeciesAddType(Index: int, SpeciesName: String, TypeName: String):
	return {"Index": Index, "SpeciesName": SpeciesName, "TypeName": TypeName}


const Type_SpeciesEditAttribute: String = "SpeciesEditAttribute"


func validateSpeciesEditAttribute(data) -> String:
	var err: String

	return ""


func createSpeciesEditAttribute(AttributeName: String, SpeciesName: String, Value: int):
	return {"AttributeName": AttributeName, "SpeciesName": SpeciesName, "Value": Value}


const Type_SpeciesEditBaseExp: String = "SpeciesEditBaseExp"


func validateSpeciesEditBaseExp(data) -> String:
	var err: String

	return ""


func createSpeciesEditBaseExp(SpeciesName: String, Value: int):
	return {"SpeciesName": SpeciesName, "Value": Value}


const Type_SpeciesEditTrainingModifier: String = "SpeciesEditTrainingModifier"


func validateSpeciesEditTrainingModifier(data) -> String:
	var err: String

	return ""


func createSpeciesEditTrainingModifier(
	SpeciesName: String, TrainingModifierName: String, Value: int
):
	return {
		"SpeciesName": SpeciesName, "TrainingModifierName": TrainingModifierName, "Value": Value
	}


const Type_SpeciesRemoveEvolution: String = "SpeciesRemoveEvolution"


func validateSpeciesRemoveEvolution(data) -> String:
	var err: String

	return ""


func createSpeciesRemoveEvolution(Id: int, SpeciesName: String):
	return {"Id": Id, "SpeciesName": SpeciesName}


const Type_SpeciesRemoveMove: String = "SpeciesRemoveMove"


func validateSpeciesRemoveMove(data) -> String:
	var err: String

	return ""


func createSpeciesRemoveMove(Id: int, SpeciesName: String):
	return {"Id": Id, "SpeciesName": SpeciesName}


const Type_SpeciesRemoveType: String = "SpeciesRemoveType"


func validateSpeciesRemoveType(data) -> String:
	var err: String

	return ""


func createSpeciesRemoveType(Index: int, SpeciesName: String):
	return {"Index": Index, "SpeciesName": SpeciesName}


const Type_StatusEditAttributeModifier: String = "StatusEditAttributeModifier"


func validateStatusEditAttributeModifier(data) -> String:
	var err: String

	return ""


func createStatusEditAttributeModifier(
	Amount: float,
	Attribute: String,
	Fixed: bool,
	Index: int,
	Permanent: bool,
	RelativeToCurrent: bool,
	Repeat: bool,
	StatusName: String
):
	return {
		"Amount": Amount,
		"Attribute": Attribute,
		"Fixed": Fixed,
		"Index": Index,
		"Permanent": Permanent,
		"RelativeToCurrent": RelativeToCurrent,
		"Repeat": Repeat,
		"StatusName": StatusName
	}


const Type_StatusRemoveAttributeModifier: String = "StatusRemoveAttributeModifier"


func validateStatusRemoveAttributeModifier(data) -> String:
	var err: String

	return ""


func createStatusRemoveAttributeModifier(Index: int, StatusName: String):
	return {"Index": Index, "StatusName": StatusName}


const Type_StatusSetAttribute: String = "StatusSetAttribute"


func validateStatusSetAttribute(data) -> String:
	var err: String

	return ""


func createStatusSetAttribute(AttributeName: String, StatusName: String, Value):
	return {"AttributeName": AttributeName, "StatusName": StatusName, "Value": Value}
