extends Node

signal error
signal types_received
signal species_received
signal status_received
signal moves_received
signal habitat_received
signal traits_received
signal conn_ready

var Messages := preload("res://messages.gd").new()

var _websocket
var _peer

func _init():
	_websocket = WebSocketClient.new()

	var error = _websocket.connect_to_url("ws://localhost:9345")
	print(error)
	
	_peer = _websocket.get_peer(1)
	_peer.set_write_mode(WebSocketPeer.WRITE_MODE_TEXT)
	
	_websocket.connect("data_received", self, "_handle_received_data")
	_websocket.connect("connection_established", self, "_on_conn_established")
	

func _process(delta):
	_websocket.poll()

func Save(path):
	_send_message(Messages.createSave(path), Messages.Type_Save)

func Load(path):
	var m = Messages.createLoad(path)
	var msg = _create_message(Messages.Type_Load, 0, m)
	_send_data(msg)
	pass
	
func AddType(type):
	var m = Messages.createAddType(type)
	_send_message(m, Messages.Type_AddType)
	
func RemoveType(type):
	var m = Messages.createRemoveType(type)
	_send_message(m, Messages.Type_RemoveType)
	
func AddTypeMatchup(atkT, defT, mod):
	_send_message(Messages.createAddTypeMatchup(atkT, defT, mod), Messages.Type_AddTypeMatchup)
	
func RemoveTypeMatchup(atkT, defT):
	_send_message(Messages.createRemoveTypeMatchup(atkT, defT), Messages.Type_RemoveTypeMatchup)
	
func GetTypeMatchups():
	_send_message(Messages.createGetTypeMatchup(), Messages.Type_GetTypeMatchup)
	
func GetSpecies():
	_send_message(Messages.createGetSpecies(), Messages.Type_GetSpecies)
	
func AddSpecies(name):
	_send_message(Messages.createAddSpecies(name), Messages.Type_AddSpecies)

func RemoveSpecies(name):
	_send_message(Messages.createRemoveSpecies(name), Messages.Type_RemoveSpecies)
	
func RenameSpecies(oldName, newName):
	_send_message(Messages.createRenameSpecies(newName, oldName), Messages.Type_RenameSpecies)
	
func SpeciesSetType(name, type, index):
	_send_message(Messages.createSpeciesAddType(index, name, type), Messages.Type_SpeciesAddType)
	
func SpeciesEditAttribute(name, attribute, value):
	_send_message(Messages.createSpeciesEditAttribute(attribute, name, value), Messages.Type_SpeciesEditAttribute)

func SpeciesEditTrainingModifier(name, attribute, value):
	_send_message(Messages.createSpeciesEditTrainingModifier(name, attribute, value), Messages.Type_SpeciesEditTrainingModifier)

func SpeciesEditBaseExp(name, value):
	_send_message(Messages.createSpeciesEditBaseExp(name, value), Messages.Type_SpeciesEditBaseExp)

func SpeciesAddEvolution(speciesName, id, name, trigger, condition, value):
	_send_message(Messages.createSpeciesAddEvolution(
		condition, id, name, speciesName, trigger, value
	), Messages.Type_SpeciesAddEvolution)
	
func SpeciesRemoveEvolution(speciesName, id):
	_send_message(Messages.createSpeciesRemoveEvolution(id, speciesName), Messages.Type_SpeciesRemoveEvolution)
	
func SpeciesAddMove(speciesName, id, name, trigger, condition, value):
	_send_message(Messages.createSpeciesAddMove(
		condition, id, name, speciesName, trigger, value
	), Messages.Type_SpeciesAddMove)
	
func SpeciesRemoveMove(speciesName, id):
	_send_message(Messages.createSpeciesRemoveMove(id, speciesName), Messages.Type_SpeciesRemoveMove)

func AddStatus(name):
	_send_message(Messages.createAddStatus(name), Messages.Type_AddStatus)

func RenameStatus(oldName, newName):
	_send_message(Messages.createRenameStatus(newName, oldName), Messages.Type_RenameStatus)
	
func RemoveStatus(name):
	_send_message(Messages.createRemoveStatus(name), Messages.Type_RemoveStatus)
	
func GetStatus():
	_send_message(Messages.createGetStatus(), Messages.Type_GetStatus)

func StatusSetAttribute(name, attributeName, value):
	_send_message(Messages.createStatusSetAttribute(attributeName, name, value), Messages.Type_StatusSetAttribute)

func StatusEditAttributeModifier(statusName, index, amount, attribute, fixed, permanent, relativeToCurrent, repeat):
	_send_message(Messages.createStatusEditAttributeModifier(amount, attribute, fixed, index, permanent, relativeToCurrent, repeat, statusName), Messages.Type_StatusEditAttributeModifier)

func StatusRemoveAttributeModifier(statusName, index):
	_send_message(Messages.createStatusRemoveAttributeModifier(index, statusName), Messages.Type_StatusRemoveAttributeModifier)


func AddMove(name):
	_send_message(Messages.createAddMove(name), Messages.Type_AddMove)

func RenameMove(oldName, newName):
	_send_message(Messages.createRenameMove(newName, oldName), Messages.Type_RenameMove)
	
func RemoveMove(name):
	_send_message(Messages.createRemoveMove(name), Messages.Type_RemoveMove)
	
func GetMove():
	_send_message(Messages.createGetMoves(), Messages.Type_GetMoves)

func MoveEditAttribute(name, attributeName, value):
	_send_message(Messages.createMoveEditAttribute(attributeName, name, value), Messages.Type_MoveEditAttribute)

func MoveEditModifier(MoveName, index, type, power, atkAttr, defAttr, status):
	_send_message(Messages.createMoveEditModifier(atkAttr, defAttr, index, MoveName, power, status, type), Messages.Type_MoveEditModifier)

func MoveRemoveModifier(MoveName, index):
	_send_message(Messages.createMoveRemoveModifier(index, MoveName), Messages.Type_MoveRemoveModifier)



func AddHabitat(name):
	_send_message(Messages.createAddHabitat(name), Messages.Type_AddHabitat)

func RenameHabitat(oldName, newName):
	_send_message(Messages.createRenameHabitat(newName, oldName), Messages.Type_RenameHabitat)
	
func RemoveHabitat(name):
	_send_message(Messages.createRemoveHabitat(name), Messages.Type_RemoveHabitat)
	
func GetHabitat():
	_send_message(Messages.createGetHabitat(), Messages.Type_GetHabitat)

func HabitatEditEntry(HabitatName, index, species, minLevel, maxLevel, chance):
	_send_message(Messages.createHabitatEditEntry(chance, HabitatName, index, maxLevel, minLevel, species), Messages.Type_HabitatEditEntry)

func HabitatRemoveEntry(HabitatName, index):
	_send_message(Messages.createHabitatRemoveEntry(HabitatName, index), Messages.Type_HabitatRemoveEntry)

func GetTraits():
	_send_message(Messages.createGetTraits(), Messages.Type_GetTraits)
	
func RenameTrait(traitName, incAttr, decAttr):
	_send_message(Messages.createSetTraitName(decAttr, incAttr, traitName), Messages.Type_SetTraitName)



func _send_message(msg, type):
	var m = _create_message(type, 0, msg)
	#print_debug(type)
	_send_data(m)
	
func _create_message(msgType, id, data):
	return {
		"Type": msgType,
		"ID": id,
		"Data": data,
	}


func _send_data(data):
	var json = JSON.print(data)
	#print(json)
	_peer.put_packet(json.to_utf8())


func _handle_received_data():
	var array = _peer.get_packet()
	var result = JSON.parse(array.get_string_from_utf8()).result
	
	match result.type:
		Messages.Type_GetTypeMatchupResponse:
			emit_signal("types_received", result.data.matchups)
		Messages.Type_GetSpeciesResponse:
			emit_signal("species_received", result.data.species)
		Messages.Type_GetStatusResponse:
			emit_signal("status_received", result.data.status)
		Messages.Type_GetMovesResponse:
			emit_signal("moves_received", result.data.moves)
		Messages.Type_GetHabitatResponse:
			emit_signal("habitat_received", result.data.habitat)
		Messages.Type_GetTraitsResponse:
			emit_signal("traits_received", result.data.traits)
		_:
				print("Unknown: " + JSON.print(result))

func _on_conn_established(_protocol):
	emit_signal("conn_ready")
