extends MenuButton

signal load_file
signal save_file

var location: String

enum Mode {Save, Load}
var mode

var popup
var fifo: File

func _ready():
	popup = get_popup()
	popup.connect("id_pressed", self, "_on_item_pressed")
	
	$FileDialog.connect("file_selected", self, "_on_file_selected")
	pass
	

func _process(_delta):
	if Input.is_action_just_pressed("save"):
		save()

func _on_item_pressed(ID):
	match ID:
		0:
			# new game
			pass
		1:
			# load
			var fd = $FileDialog
			fd.mode = FileDialog.MODE_OPEN_FILE
			mode = Mode.Load
			fd.current_dir = "/home/kori/dev/go/src/gitlab.com/kori-irrlicht/promo-tool/assets/"
			fd.popup()
			
			pass
		2:
			# save as
			_save_as()
		3: 
			save()
			
		5:
			get_tree().quit()
			
func save():
	if location:
		Promotool.Save(location)
	else:
		_save_as()
			
func _save_as():
	var fd = $FileDialog
	fd.mode = FileDialog.MODE_SAVE_FILE
	mode = Mode.Save
	fd.current_dir = "/home/kori/dev/go/src/gitlab.com/kori-irrlicht/promo-tool/assets/"
	fd.popup()

func _on_file_selected(file):
	location = file
	match mode:
		Mode.Load:
			Promotool.Load(location)
		Mode.Save:
			Promotool.Save(location)

